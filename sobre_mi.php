<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */


require "./include/template.php";

template_config("Sobre mí", __FILE__);

head();
?>
<div class="container-sm">
    <div class="bloque mx-auto">
        <div class="titulito">
            ¿Por qué este sitio?
        </div>
        <p>
            En parte porque quería poner a prueba mis habilidades y para crear un pequeño rinconcito del mundo donde publicar lo que me gusta, y una red social no me parecía lo suficientemente organizada para hacerlo.  
        </p>

        <div class="titulito">
            Me gusta:
        </div>
        <ul>
            <li>los videojuegos 👾</li>
            <li>el software libre 💡</li>
            <li>la música 🎧️</li>
            <li>las artes 🧐</li>
            <li>los computadores 💾</li>
        </ul>
    </div>
</div>
<?php
pies();