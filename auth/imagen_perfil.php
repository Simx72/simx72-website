<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */
//

require_once __DIR__ . '/../include/auth.php';
require_once __DIR__ . '/../include/db.php';

function sendImg($mime, $value) {
	header('Content-Type: ' . $mime);
	echo $value;
	die;
}

function default_img() {
	$image = get_userimage_by_id(-1);
	sendImg($image['mime'], $image['value']);
}

if (isset($_GET["id"])) {
	$id = hexdec($_GET["id"]);
	$image = get_userimage_by_id($id);
	if ($image) {
		sendImg($image['mime'], $image['value']);
	}
} else if ($auth->isLoggedIn()) {
	$id = $auth->getUserId();
	$image = get_userimage_by_id($id);
	if ($image) {
		sendImg($image['mime'], $image['value']);
	}
}
default_img();