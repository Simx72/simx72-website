<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */

use Delight\Auth\InvalidPasswordException;
use Delight\Auth\NotLoggedInException;
use Delight\Auth\TooManyRequestsException;

require_once __DIR__ . '/../include/auth.php';
require_once __DIR__ . '/../include/template.php';


template_config('Mi cuenta', __FILE__);

head();


if (!$auth->isLoggedIn()) {
	?>
	<div class="container">
		Primer necesitas autenticarte. Para hacerlo ve a <a href="/auth/ingreso.php">Ingreso</a>.
	</div>
	<?php
	pies();
	die;
}

$accion = $_POST["accion"] ?? "";

?>

<div class="container">
	<h1 class="text-center titulo">Mi cuenta</h1>
	<br>
	<div class="row flex-row-reverse justify-content-around w-100">
		<div class="col-md-6">
			<div class="bloque mb-5">
				<p>
					Bienvenido
					<?php echo $auth->getUsername(); ?>
				</p>
			</div>
			<div class="bloque mb-5">
				<?php
				if ($accion == "cambiar-clave") {
					if (isset ($_POST["clave-vieja"]) && isset ($_POST["clave-nueva"]) && isset ($_POST["clave-nueva-2"])) {
						try {
							$clave_vieja = $_POST["clave-vieja"];
							$clave_nueva = $_POST["clave-nueva"];
							$clave_nueva2 = $_POST["clave-nueva-2"];
							if ($auth->reconfirmPassword($clave_vieja)) {
								if ($clave_nueva != $clave_nueva2) {
									throw new Exception("Las claves no coinciden");
								} else {
									$auth->changePassword($clave_vieja, $clave_nueva);
									alerta("Clave cambiada con éxito");
								}
							} else {
								throw new Exception("Las clave vieja no es la correcta");
							}
						} catch (NotLoggedInException $e) {
							alerta_error("El usuario no esta registrado");
						} catch (TooManyRequestsException $e) {
							alerta_error("Demasiadas solicitudes en muy poco tiempo");
						} catch (InvalidPasswordException $e) {
							alerta_error("La nueva clave no es válida");
						} catch (Exception $e) {
							alerta_error($e->getMessage());
						}
					}
				}
				?>
				<form action="cuenta.php" method="post">
					<div class="titulito">Cambiar clave</div>
					<input type="hidden" name="accion" value="cambiar-clave" readonly>
					<br>
					<div class="form-floating mb-3">
						<input type="password" class="form-control" id="clave-vieja" name="clave-vieja"
							placeholder="Clave vieja" required>
						<label for="clave-vieja">Clave vieja</label>
					</div>
					<div class="form-floating mb-3">
						<input type="password" class="form-control" id="clave-nueva" name="clave-nueva"
							placeholder="Clave nueva" required>
						<label for="clave-nueva">Clave nueva</label>
					</div>
					<div class="form-floating mb-3">
						<input type="password" class="form-control" id="clave-nueva-2" name="clave-nueva-2"
							placeholder="Clave nueva-2" required>
						<label for="clave-nueva-2">Repite clave nueva</label>
					</div>
					<button class="btn btn-primary w-100" type="submit">
						Cambiar
						<i class="bi bi-arrow-right-short"></i>
					</button>
				</form>
			</div>
		</div>
		<div class="col-md-6">
			<div class="bloque mb-5 mx-5">
				<?php
				if ($accion == "cambiar-imagen") {
					try {
						if (isset ($_FILES['imagen'])) {
							$file = $_FILES['imagen'];
							$check = getimagesize($file["tmp_name"]);
							if ($check !== false) {
								$id = $auth->getUserId();
								$data = file_get_contents($file['tmp_name']);
								$mime = $check['mime'];
								$size = $file['size'] / 1_000_000; // megabytes
								if ($size < 1.5/* Mb */) {
									set_user_image($id, $mime, $data);
									alerta("Imágen cambiada con éxito");
								} else {
									throw new Exception("Archivo muy grande.\nMaximo permitido = 1.5Mb");
								}
							} else {
								throw new Exception("El archivo no es una imagen");
							}
						} else {
							throw new Exception("No se recibió el archivo");
						}
					} catch (Exception $e) {
						alerta_error($e->getMessage());
					}
				}
				?>
				<img class="img-de-perfil rounded-circle" src="./imagen_perfil.php"
					alt="Imagen de perfil de <?php echo $auth->getUsername(); ?>" />
				<br>
				<form action="cuenta.php" method="post" enctype="multipart/form-data">
					<input type="hidden" name="accion" value="cambiar-imagen">
					<input type="file" style="visibility:hidden;" id="imagen-perfil" name="imagen" required
						aria-label="Cambiar imagen">
					<div class="input-group mb-3" style="display: flex; flex-wrap: nowrap; justify-content:center;">
						<label class="btn btn-secondary puntos-suspensivos" style="max-width: 160px;"
							for="imagen-perfil" id="elegir-imagen">
							Elegir imagen
						</label>
						<button class="btn btn-primary" type="submit" id="subir-imagen-perfil">
							Cambiar
							<i class="bi bi-arrow-repeat"></i>
						</button>
					</div>
				</form>
				<div class="w-100 text-center nodis">
					formato: jpeg o png
				</div>

			</div>
		</div>
	</div>
	<br><br>

	<div class="d-grid gap-2">
		<a href="./cerrar_sesion.php" class="btn btn-danger text-white" type="button">Cerrar sesión</a>
	</div>
</div>


<script type="module" src="/static/js/cuenta.js"></script>

<?php

pies();


