#!/usr/bin/env php
<?php
if ($argc < 3) {
    throw new Exception('Not enough arguments');
}

// T*T*T*T*T*

require_once __DIR__ . "/../vendor/scssphp/scssphp/scss.inc.php";
use ScssPhp\ScssPhp\Compiler;

$compiler = new Compiler();

$source_path = $argv[1];
$destin_path = $argv[2];
$verbose = $argv[3] ?? false;

function debug(string $text) {
    global $verbose;
    if ($verbose == "-v") {
        echo "[debug] ".$text."\n";
    }
}

// read sources to $source
$compilados = array(/* name, contents */);
if (is_file($source_path)) {
    debug("compiling: " . $source_path);
    $file = $compiler->compileFile($source_path);
    array_push($compilados, $file);
} else if (is_dir($source_path)) {
    if ($dh = opendir($source_path)) {
        while (($filename = readdir($dh)) !== false) {
            if (in_array($filename, [".", ".."]) || strpos($filename, "_") === 0) {
                continue;
            }
            debug("compiling: " . $source_path . "/" . $filename);
            $file = $compiler->compileFile($source_path . "/" . $filename);
            array_push($compilados, ["css" => $file->getCss(), "name" => $filename]);
        }
        closedir($dh);
    }
} else {
    throw new Exception("Que paso aquí? >:v");
}

// debug("compiled_array:");
// file_put_contents(__DIR__."/debug_array.json", json_encode($compilados));

// write to corresponding files 
if (is_dir($destin_path)) {
    foreach ($compilados as $compiled) {
        $css = $compiled["css"];
        $name = explode(".", $compiled["name"])[0].".css";
        debug("writing: ".$destin_path."/".$name);
        file_put_contents(
            $destin_path."/".$name,
            $css
        );
    }
} else {
    //write to given file
    foreach ($compilados as $compiled) {
        $css = $compiled["css"];
        debug("writing: ".$destin_path);
        file_put_contents($destin_path, $css);
    }
}


echo "\n";