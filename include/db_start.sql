-- First of all, the config table

CREATE TABLE `config_table` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `key` varchar(45) NOT NULL,
  `value` varchar(90) DEFAULT NULL
);
INSERT INTO `config_table` (`key`, `value`) VALUES
('smtp_host', 'host.example.com'),
('smtp_password', ''),
('smtp_port', ''),
('smtp_security', 'ssl'),
('smtp_username', ''),
('smtp_apodo', ''),
('smtp_email', ''),
('site_name', ''),
('site_hostname', '');

-- timezon set
SET time_zone='+00:00';

-- assets

CREATE TABLE `assets` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `key` varchar(45) NOT NULL,
  `value` longblob DEFAULT NULL,
  `mime` varchar(45) DEFAULT 'application/octet-stream'
);
INSERT INTO `assets` (`id`, `key`, `value`, `mime`) VALUES
(1, 'default_profil_img', NULL, 'image/jpeg');
INSERT INTO `assets` (`id`, `key`, `value`, `mime`) VALUES
(2, 'background', NULL, 'image/jpeg');
-

-- juegos

CREATE TABLE `juegos` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `nombre` varchar(45) DEFAULT NULL,
  `img` longblob DEFAULT NULL,
  `imgmime` varchar(45) DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `href` varchar(60) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT 0
);


-- blog

CREATE TABLE blog (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `headerimg` longblob DEFAULT NULL,
  `headerimgmime` varchar(45) DEFAULT NULL,
  `content` text NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT 0,
  `css` text DEFAULT "",
  `owner` int(11) NOT NULL,
  `fecha_creado` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_publicado` TIMESTAMP NULL DEFAULT NULL,
  `fecha_ultimo_cambio` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);



-- users

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `email` varchar(249) NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT 0,
  `verified` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `resettable` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `roles_mask` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `registered` int(10) UNSIGNED NOT NULL,
  `last_login` int(10) UNSIGNED DEFAULT NULL,
  `force_logout` mediumint(7) UNSIGNED NOT NULL DEFAULT 0,
  `imgid` int(11) DEFAULT NULL,
  `edit_juegos` tinyint(1) DEFAULT 0 NOT NULL,
  `edit_blog` tinyint(1) DEFAULT 0 NOT NULL
);
CREATE TABLE `users_confirmations` (
  `id` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(249) NOT NULL,
  `selector` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `expires` int(10) UNSIGNED NOT NULL
);
CREATE TABLE `users_images` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `mime` varchar(45) NOT NULL,
  `value` longblob NOT NULL
);
CREATE TABLE `users_remembered` (
  `id` bigint(20) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user` int(10) UNSIGNED NOT NULL,
  `selector` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `expires` int(10) UNSIGNED NOT NULL
);
CREATE TABLE `users_resets` (
  `id` bigint(20) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user` int(10) UNSIGNED NOT NULL,
  `selector` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `expires` int(10) UNSIGNED NOT NULL
);
CREATE TABLE `users_throttling` (
  `bucket` varchar(44) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `tokens` float UNSIGNED NOT NULL,
  `replenished_at` int(10) UNSIGNED NOT NULL,
  `expires_at` int(10) UNSIGNED NOT NULL
);
