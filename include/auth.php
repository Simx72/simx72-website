<?php
use Delight\Auth\AuthError;

/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */

require_once __DIR__ . "/./env.php";
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../include/db.php';
require_once __DIR__ . '/../include/mailer.php';

$auth = new \Delight\Auth\Auth($db);

$challenge_available_algorithms = array(
	"SHA-256" => "sha256",
	"SHA-384" => "sha384",
	"SHA-512" => "sha512"
);

function crear_challenge(string|null $salt, int|null $secret_number)
{
	global $env, $challenge_available_algorithms;

	$salt = $salt ?? bin2hex(random_bytes(12));
	$secret_number = $secret_number ?? random_int(100_000, 350_000);

	$algorithm = $challenge_available_algorithms[$env['ALTCHA_ALGORITHM']];

	$challenge = hash($algorithm, $salt . $secret_number);

	$signature = hash_hmac($algorithm, $challenge, $env['ALTCHA_HMAC_KEY']);

	return ['algorithm' => $env['ALTCHA_ALGORITHM'], 'challenge' => $challenge, 'signature' => $signature, 'salt' => $salt];
}

function altcha_es_valido(string $altcha): bool
{
	$json = json_decode(base64_decode($altcha), true);

	if ($json !== null) {
		$check = crear_challenge($json['salt'], $json['number']);

		// var_dump($json, $check);

		return $json['algorithm'] === $check['algorithm']
			&& $json['challenge'] === $check['challenge']
			&& $json['signature'] === $check['signature'];
	}
	return false;
}


function verificar_conexion()
{
	if (
		!(isset ($_SERVER['HTTPS']) &&
			($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
			isset ($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
			$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
	) {
		?>
		<div class="alert alert-danger" role="alert" id="https-warning">
			<div>
				<h3>Cuidado</h3>
			</div>
			<div>
				La conexión no esta protegida por el protocolo HTTPS. La información podría ser robada en tránsito
			</div>
		</div>
		<script>
			{
				let btn = document.createElement('button');
				btn.type = "button";
				btn.classList.add("btn", "btn-success");
				btn.innerHTML = 'Proteger <i class="bi bi-lock"></i>';
				let secureUrl = new URL(window.location.href);
				secureUrl.protocol = "https";
				btn.addEventListener('click', _ => {
					window.location.url = secureUrl;
				})
				let div = document.createElement('div');
				div.appendChild(btn);
				document.getElementById('https-warning').appendChild(div);
			}
		</script>
		<?php
	}
}

function get_username_by_id(int $id)
{
	global $db;
	$query_string = "SELECT `username` FROM `users` WHERE `id` = ?";
	$query = $db->prepare($query_string);
	$ok = $query->execute([$id]);
	if ($ok) {
		$res = $query->fetch();
		if ($res) {
			return $res["username"];
		}
	}
	return false;
}

/**
 * @return array{"mime": string, "value": string} | null
 */
function get_userimage_by_id(int $id): array|null
{
	global $db;
	if ($id == -1) {
		$query_string = "SELECT `mime`, `value` FROM `users_images` WHERE `key` = ?";
		$query = $db->prepare($query_string);
		$ok = $query->execute(['default_profil_img']);
		if ($ok) {
			$res = $query->fetch();
			if ($res) {
				return $res;
			}
		}
	} else {
		$query_string = "SELECT `mime`, `value` FROM `users_images` WHERE `id` = ?";
		$query = $db->prepare($query_string);
		$ok = $query->execute([$id]);
		if ($ok) {
			$res = $query->fetch();
			if ($res) {
				return $res;
			}
		}
	}
	return null;
}


function set_user_image(int $id, string $mime, string $value)
{
	global $db;
	$error_text = "Ocurrio un error cambiando la imagen.\n";
	$image_exists = get_userimage_by_id($id);
	// var_dump($image_exists);
	if ($image_exists) {
		$query_string = "UPDATE `users_images` SET `mime` = ? , `value` = ? WHERE `id` = ?";
		$query = $db->prepare($query_string);
		$ok = $query->execute([$mime, $value, $id]);
		if ($ok) {
			return true;
		} else {
			throw new Exception($error_text . $query->errorInfo()[2]);
		}
	} else {
		$query_string = "INSERT INTO `users_images` (`id`, `mime`, `value`) VALUES (?, ?, ?)";
		$query = $db->prepare($query_string);
		$ok = $query->execute([$id, $mime, $value]);
		if ($ok) {
			return true;
		} else {
			throw new Exception($error_text . $query->errorInfo()[2]);
		}
	}
}


// blog

class BlogNotAllowedException extends Exception {}

function auth_may_edit_blog(): bool
{
	global $db, $auth;
	if ($auth->isLoggedIn()) {
		$id = $auth->getUserId();
		$query_string = "SELECT `edit_blog` FROM `users` WHERE `id` = ?";
		$query = $db->prepare($query_string);
		$ok = $query->execute([$id]);
		if ($ok) {
			$res = $query->fetch();
			return $res["edit_blog"];
		} else {
			throw new Exception($query->errorInfo()[2]);
		}
	} else {
		return false;
	}
}

/**
 * @return array{"id": int, "titulo": string, "content": string, "headerimg":string, "headerimgmime":string, "hidden":bool, "css":string, "owner":int, "fecha_creado":string, "fecha_publicado":string, "fecha_ultimo_cambio": string} | null
 */
function auth_get_post(int $id)
{
	global $db;
	$query_string = "SELECT * FROM `blog` WHERE `id` = ?";
	$query = $db->prepare($query_string);
	$ok = $query->execute([$id]);
	if ($ok) {
		$res = $query->fetch();
		return $res;
	} else {
		throw new Exception("Ocurrió un error con la consulta SQL: " . $query->errorInfo()[2]);
	}
}

function auth_check_post($userid, $post)
{
	return $post["owner"] == $userid;
}

function auth_get_post_with_check($postid, $userid)
{
	$post = auth_get_post($postid);
	if (!$post) {
		throw new Exception("La publicación no existe");
	}
	if (!auth_check_post($userid, $post)) {
		throw new BlogNotAllowedException("El usuario no tiene permiso de editar esta publicación");
	}
	return $post;
}