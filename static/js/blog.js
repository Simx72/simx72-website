/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */
//

import '../../vendor/quilljs/quill/quill.min.js';
import { ajax } from "./funciones.js";

window.addEventListener('DOMContentLoaded', _ => {
    document.getElementById('quill-editor').style.display = "block"
    const editor = new Quill('#editor', {
        debug: false,
        modules: {
            toolbar: true,
        },
        placeholder: 'Compose an epic...',
        theme: 'snow'
    });
    ajax("POST", "./update_content.php", {
        accion: 'get',
        id: new URL(window.location.href).searchParams.get('id')
    }).then((xml) => {

        editor.setContents(
            JSON.parse(xml.responseText)
        )
        badgeSync(false);
    });
    const bSincro = document.getElementById('b-sincro');
    const bActual = document.getElementById('b-actual');
    function badgeSync(sync) {
        if (sync) {
            bActual.style.display = "none";
            bSincro.style.display = "inline-block";
        } else {
            bActual.style.display = "inline-block";
            bSincro.style.display = "none";
        }
    }
    let lastUpdateTimeout = 0;
    function update() {
        let body = {
            id: new URL(window.location.href).searchParams.get('id'),
            delta: JSON.stringify(editor.getContents())
        };
        ajax("POST", "./update_content.php", body).then((xml) => {

            // console.log(xml.responseText)

            badgeSync(false);

        });
    }
    editor.on('text-change', (/* delta, oldDelta, source */) => {
        clearTimeout(lastUpdateTimeout);
        lastUpdateTimeout = setTimeout(() => update(), 1500);
        badgeSync(true);
    });

    // hacer-publico-form
    /** @type {HTMLFormElement[]} */
    let HCForms = [document.getElementById('hacer-publico-form-false'), document.getElementById('hacer-publico-form-true')];
    HCForms.map((form, i) => form.addEventListener('submit', ev => {
        ev.preventDefault();
        let formData = Object.fromEntries(new FormData(form));
        ajax("POST", window.location.href, formData).then(_ => {
            if (i == 0) {
                HCForms[0].style.display = "none";
                HCForms[1].style.display = "block";
            } else {
                HCForms[0].style.display = "block";
                HCForms[1].style.display = "none";
            }
        })
    }))

    // eliminar form
    let eliminarForm = document.getElementById('eliminar-form');
    eliminarForm.addEventListener('submit', ev => {
        ev.preventDefault();
        let check = confirm("¿Esta seguro que quiere eliminar esta publicación?");
        if (check) {
            let formData = Object.fromEntries(new FormData(eliminarForm));
            ajax("POST", window.location.href, formData).then(_ => {
                window.location.pathname = "/blog/index.php";
            });
        }
    })
})