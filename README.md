# Simx72-website

## Archivado

Este repositorio ya no va a recibir más actualizaciones. Sin embargo el código sigue estando disponible por si alguien desea usarlo 

--------------------

Este es un sitio personal con blog, pagina de juegos y más ...

Usa `php`, `composer`, `pdo`, `sass`, `smtp`.

Esta publicado bajo la licencia GNU-AGPL-v3 así que sientete libre de copiarlo para crear tu sitio


## Instalación - # pasos


### 1. Puedes clonar el repositorio con

```bash
git clone https://codeberg.org/Simx72/simx72-website [carpeta]
```

### 2. Luego, instala las dependencias

```bash
composer install
```

### 3. Añade un archivo `include/.ht.env` con lo siguiente:
**(Cambiando en el archivo lo que esta entre paréntesis por los valores que necesitas)**
```ini
DB_CONNECT="mysql:dbname=(nombre_de_la_base_de_datos);host=(host_de_la_base_de_datos);charset=utf8mb4 (usuario) (constraseña)"
MODE="production"

# Importante, una especie de contraseña para que el captcha funcione
ALTCHA_HMAC_KEY="(llave_secreta)"
# Optional, defaults to SHA-256. Can be SHA-384 or SHA-512
ALTCHA_ALGORITHM="SHA-256"
NOMBRE_SITIO="(nombre_del_sitio)"
```
*DB_CONNECT puede ser cualquier string de conexión valida para PDO*

### 4. Necesitas crear las bases de datos en tu servidor
Un ejemplo para mysql esta en `include/db_start.sql` 

### 5. Edita los valores de la tabla `config_table` de la base de datos

puedes hacerlo con phpmyadmin

### Y listo!

El sitio esta listo :)


## Para desarrolladores

Hay un script dev que compila los estilos de la carpeta sass

```bash
npm install
npm run dev
```
