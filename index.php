<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */



require_once __DIR__ . '/include/template.php';
require_once __DIR__ . '/include/db.php';

template_config("Inicio", __FILE__);

$site_config = getSiteConfig();

head();

?>
	<div class="container-sm">
		<div class="bloque mx-auto">
			Holaa, bienvenido a <?php echo $site_config["site_name"][0]; ?>. <br>
			Acá publico de todo un poco sobre las cosas que me gustan y que veo día a día
		</div>
		<div class="row justify-content-center">
		</div>
	</div>
    
<?php

pies();