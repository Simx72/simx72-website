<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */


require_once __DIR__ . '/include/template.php';
require_once __DIR__ . '/include/db.php';

head();

try {
    if (!isset($_GET["username"])) {
        throw new Exception("No username provided");
    }
    $username = $_GET["username"];
    ?>
    <div class="container-sm">
        <?php
        if ($username == "Simx72") {
            ?>
            Bienvenido, Soy Simx72 <br>
            Mas información en el enlace: <br>
            Mastodon: <a href="https://tkz.one/@Simx72" target="_blank">@Simx72@tkz.one</a>
            <?php
        } else {
            ?>
            
            <?php
        }
        ?>
    </div>
    <?php
} catch (Exception $e) {

}

pies();