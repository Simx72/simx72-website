<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */
//

require_once __DIR__ . '/../include/auth.php';


if (!$auth->isLoggedIn()) {
    http_response_code(403);
    echo "Debes autenticarte primero";
    die;
}


if (!isset ($_POST["id"])) {
    http_response_code(400);
    echo "No hay suficientes parametros";
    die;
}

$postid = hexdec($_POST["id"]);
$userid = $auth->getUserId();

if (isset ($_POST["accion"])) {
    if ($_POST["accion"] == "get") {
        $query_string = "SELECT `content` FROM `blog` WHERE `id` = ? AND `owner` = ?";
        $query = $db->prepare($query_string);
        $ok = $query->execute([$postid, $userid]);
        if ($ok) {
            $res = $query->fetch();
            if ($res) {
                header('Content-Type: application/json');
                echo $res['content'];
                die;
            }
        }
        http_response_code(500);
        echo "Ocurrió un error en el servidor";
        die;
    }
}


if (!isset($_POST["delta"])) {
    http_response_code(400);
    echo "No hay suficientes parametros";
    die;
}

$query_string = "UPDATE `blog` SET `content` = ?, `fecha_ultimo_cambio` = now() WHERE `id` = ? AND `owner` = ?";
$query = $db->prepare($query_string);
$ok = $query->execute([$_POST["delta"], $postid, $userid]);
if (!$ok) {
    http_response_code(500);
    echo "Ocurrió un error en el servidor";
    die;
}

header('Location: ./editar_post.php?id='.dechex($postid));
echo "good";

