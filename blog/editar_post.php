<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */
//

require_once __DIR__ . "/../include/template.php";
require_once __DIR__ . "/../include/auth.php";

template_config("Editar post", __FILE__);

head();


// si loggedIn & Permiso de blog & post le pertenece
try {
    if (!$auth->isLoggedIn()) {
        throw new Exception("No ha iniciado sesión");
    }
    if (!auth_may_edit_blog()) {
        throw new Exception("No tienes permiso para editar publicaciones");
    }
    if (!isset($_GET["id"])) {
        throw new Exception("El parametro 'id' no esta definido");
    }
    $postid = hexdec($_GET["id"]);
    $userid = $auth->getUserId();

    // UPDATE TABLE `blog` si $_POST["accion"] == ??
    $accion = $_POST["accion"] ?? "";

    // obtener información del post
    $post = auth_get_post_with_check($postid, $userid);

    // aplicación:
    ?>
    <div class="container-sm">
        <div class="mb-3">
            <a href="./index.php" class="btn btn-info sin-estilo">
                <i class="bi bi-arrow-left-short"></i>
                Volver
            </a>
        </div>
        <small>
            Fecha creado:&Tab;
            <?php echo $post["fecha_creado"]; ?> <br>
            Fecha ultimo cambio:&Tab;
            <?php echo $post["fecha_ultimo_cambio"]; ?> <br>
            ID:&Tab;
            <?php echo $post["id"]; ?> <br>
        </small>

        <?php
        try {
            if ($accion == "actualizar-titulo") {
                if (isset($_POST["titulo"])) {
                    $titulo = str_replace("\"", "", $_POST["titulo"]);
                    $query_string = "UPDATE `blog` SET `titulo` = ? WHERE `id` = ? AND `owner` = ?";
                    $query = $db->prepare($query_string);
                    $ok = $query->execute([$titulo, $postid, $userid]);
                    if ($ok) {
                        echo '<br/>';
                        alerta("El título fue cambiado con éxito");
                        $post['titulo'] = $titulo;
                    } else {
                        throw new Exception("Ocurrió un error con la consulta SQL: " . $query->errorInfo()[2]);
                    }
                }
            }
        } catch (Exception $e) {
            alerta_error($e->getMessage());
        }
        ?>
        <form action="" method="post">
            <div class="row my-5">
                <div class="col-md-6">
                    <input type="hidden" name="accion" value="actualizar-titulo" readonly>
                    <br>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título" required
                            value="<?php
                            echo $post["titulo"];
                            ?>">
                        <label for="titulo">Título</label>
                    </div>
                    <button class="btn btn-primary">
                        Actualizar título
                        <i class="bi bi-arrow-return-left"></i>
                    </button>
                </div>
            </div>
        </form>


        <?php /* Editor de contenido */ ?>
        <link rel="stylesheet" href="/vendor/quilljs/quill/quill.core.css">
        <link rel="stylesheet" href="/vendor/quilljs/quill/quill.snow.css">
        <link rel="stylesheet" href="/build/css/quill.css">

        <div class="quill-editor" id="quill-editor" style="display: none;">
            <span id="b-sincro" class="badge text-bg-warning m-2">
                Sincronizando
                <div class="spinner-border spinner-border-sm" role="status">
                    <span class="visually-hidden">...</span>
                </div>
            </span>
            <span id="b-actual" class="badge text-bg-success m-2" style="display: none;">
                Sincronizado
                <i class="bi bi-check-circle"></i>
            </span>
            <div id="editor"></div>
        </div>
        <script type="module" src="/static/js/blog.js"></script>
        <noscript>
            <small>Habilite javascript para una mejor experiencia</small>
            <small><b>El texto febe ser un objeto 'Delta' de Quill válido, o la página tendrá problemas para
                    cargar</b></small>
            <form action="./update_content.php" method="post">
                <input type="hidden" name="id" value="<?php echo $postid; ?>">
                <textarea name="delta" cols="30" rows="10" style="font-family: monospace;">
                                        <?php echo $post["content"]; ?>
                                    </textarea><br>
                <button type="submit" class="btn btn-success">
                    Actualizar
                    <i class="bi bi-file-earmark-arrow-up"></i>
                </button>
            </form>
        </noscript>




        <div class="row justify-content-between mt-5">
            <div class="col-md-2 mb-2">
                <?php
                if ($accion == "hacer-publico") {
                    try {
                        if (isset($_POST["publico"])) {
                            $oculto = 0;
                            if ($_POST["publico"] == "false") {
                                $oculto = 1;
                            } else if ($_POST["publico"] == "true") {
                                $oculto = 0;
                            } else {
                                throw new Exception("Valor del argumento `publico` no válido");
                            }
                            $query_string = "UPDATE `blog` SET `hidden` = ? WHERE `id` = ? AND `owner` = ?";
                            $query = $db->prepare($query_string);
                            $ok = $query->execute([$oculto, $postid, $userid]);
                            if ($ok) {
                                echo '<br/>';
                                alerta("Visibilidad cambiada con éxito");
                                $post['hidden'] = $oculto;
                            } else {
                                throw new Exception("Ocurrió un error con la consulta SQL: " . $query->errorInfo()[2]);
                            }
                        }
                    } catch (Exception $e) {
                        alerta_error($e->getMessage());
                    }
                }
                ?>
                <form action="" method="post" id="hacer-publico-form-true" <?php echo $post["hidden"] ? "" : 'style="display:none;"'; ?>>
                    <input type="hidden" name="accion" value="hacer-publico">
                    <input type="hidden" name="publico" value="true">
                    <span class="badge mb-1 text-bg-warning">
                        Oculto
                    </span>
                    <button type="submit" class="btn btn-info">
                        Publicar
                        <i class="bi bi-file-earmark-arrow-up"></i>
                    </button>
                </form>
                <form action="" method="post" id="hacer-publico-form-false" <?php echo $post["hidden"] ? 'style="display:none;"' : ""; ?>>
                    <input type="hidden" name="accion" value="hacer-publico">
                    <input type="hidden" name="publico" value="false">
                    <span class="badge mb-1 text-bg-success">
                        Público
                    </span>
                    <button type="submit" class="btn btn-info">
                        Ocultar
                        <i class="bi bi-file-earmark-lock2"></i>
                    </button>
                </form>
            </div>
            <div class="col-md-2 mb-2">
                <a href="./post.php?id=<?php echo dechex($postid); ?>" class="btn btn-secondary sin-estilo">
                    Mostrar
                    <i class="bi bi-eye-fill"></i>
                </a>
            </div>
            <?php
            if ($accion == "eliminar") {
                try {
                    $query_string = "DELETE FROM `blog` WHERE `id` = ? AND `owner` = ?";
                    $query = $db->prepare($query_string);
                    $ok = $query->execute([$postid, $userid]);
                    if ($ok) {
                        echo '<br/>';
                        alerta("Eliminado con éxito");
                        header("Location: ./index.php");
                        ?>
                        <span>
                            Eliminado!
                            <a href="./index.php">Ir a inicio</a>
                        </span>
                        <?php
                    } else {
                        throw new Exception("Ocurrió un error con la consulta SQL: " . $query->errorInfo()[2]);
                    }
                } catch (Exception $e) {
                    alerta_error($e->getMessage());
                }
            }
            ?>
            <div class="col-md-2 mb-2">
                <form action="" method="post" id="eliminar-form">
                    <input type="hidden" name="accion" value="eliminar">
                    <button type="submit" class="btn btn-danger">
                        Eliminar
                        <i class="bi bi-trash3-fill"></i>
                    </button>
                </form>
            </div>
        </div>



    </div>
    <?php
} catch (Exception $e) {
    alerta_error($e->getMessage(), "./index.php");
}

pies();