<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */
//

// crea nuevo post y redirige a la edición

require_once __DIR__ . "/../include/auth.php";
require_once __DIR__ . "/../include/db.php";

try {
	if (!$auth->isLoggedIn()) {
		throw new Exception("Para crear un post debe iniciar sesión primero");
	}
	if (!auth_may_edit_blog()) {
		throw new Exception("No tienes permiso para crear un post " . $auth->getUsername());
	}
	$userid = $auth->getUserId();
	$query_string = "INSERT INTO `blog` (`content`, `hidden`, `css`, `owner`) VALUES (?, ?, ?, ?)";
	$query = $db->prepare($query_string);
	$ok = $query->execute(["", 1, "", $userid]);
	if (!$ok) {
		$errormsg = $query->errorInfo()[2];
		error_log($errormsg);
		http_response_code(500);
		echo 'Ocurrió un error en el servidor.';
	}
	$rowid = $db->lastInsertId();
	header('Location: ./editar_post.php?id=' . dechex($rowid));
	echo 'Post creado, redirigiendolo al editor ...';
} catch (Exception $e) {
	require_once __DIR__ . "/../include/template.php";
	head();
	alerta_error($e->getMessage(), "./index.php");
	pies();
}
