<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */
//

require_once __DIR__ . "/../include/template.php";
require_once __DIR__ . "/../include/auth.php";
require_once __DIR__ . "/../vendor/autoload.php";

template_config("Post", __FILE__);

$site_config = getSiteConfig();


// mostrar publicacion segun el $_GET["id"]  T*T
try {
    if (!isset($_GET["id"])) {
        throw new Exception("No hay un parametro id");
    }
    $postid = hexdec($_GET["id"]);
    $post = auth_get_post($postid);
    if (!$post) {
        http_response_code(404);
        head();
        alerta_error("No encontramos la publicación #" . $postid, "./index.php", "Parece que la publicación que busca no existe");
        pies();
        die;
    }
    head();
    ?>
    <div class="container-sm">
        <link rel="stylesheet" href="../build/css/blog.css">
        <?php
        if ($post["hidden"]) {
            if (!$auth->isLoggedIn()) {
                throw new Exception("Debe autenticarse para ver esta publicación");
            }
            $userid = $auth->getUserId();
            if ($userid !== $post["owner"]) {
                throw new Exception("No puede ver una publicación privada, " . $auth->getUsername());
            }
            // si sí le pertenece, continúa
        }
        $userid = $auth->getUserId();
        if ($userid === $post["owner"]) {
            ?>
            <div class="row mb-5 justify-content-between">
                <div class="col-md-6">
                    <small>
                        Fecha creado:&Tab;
                        <?php echo $post["fecha_creado"]; ?> <br>
                        Fecha ultimo cambio:&Tab;
                        <?php echo $post["fecha_ultimo_cambio"]; ?> <br>
                        ID:&Tab;
                        <?php echo $post["id"]; ?> <br>
                    </small>
                </div>
                <div class="col-md-3">
                    <a href="editar_post.php?id=<?php echo dechex($postid); ?>" class="btn btn-info sin-estilo mt-2 ms-2">
                        Editar
                        <i class="bi bi-pen-fill"></i>
                    </a>
                </div>
            </div>
            <?php
        }
        function licencia()
        {
            global $site_config, $post, $postid;
            ?>
            <small class="d-block m-3">
                <b>
                    <?php
                    $hostname = $site_config['site_hostname'][0];
                    $owner = get_username_by_id($post["owner"]);
                    ?>
                    "<a href="https://<?php echo $hostname; ?>/blog/post.php?id=<?php echo dechex($postid); ?>">Publicación #
                        <?php echo $postid; ?>
                    </a>"
                    por
                    "<a href="https://<?php echo $hostname; ?>/user.php?username=<?php echo urlencode($owner); ?>">
                        <?php echo $owner; ?>
                    </a>"
                    esta publicado bajo la licencia
                    <a href="https://creativecommons.org/licenses/by-nc-nd/4.0/">CC-BY-NC-ND 4.0</a>.
                </b>
            </small>
            <?php
        }
        // mostrar post
        licencia();
        ?>
        <div class="titulo">
            <?php echo $post["titulo"] ?>
        </div>
        <div class="content">
            <?php
            try {

                $content = $post["content"];

                if ($content == "") {
                    $content = '{"ops":[{"insert":"[vacío]"}]}';
                }

                // Create the lexer object with your given Quill JSON delta code (either PHP array or JSON string).
                $lexer = new \nadar\quill\Lexer($content);

                // Echo the HTML for the given JSON ops.
                echo $lexer->render();
            } catch (TypeError $e) {
                alerta_error("Ocurrió un error al mostrar esta publicación", "", "Al parecer la publicación esta defectuosa, inténtelo de nuevo mas tarde.<br>Error: " . $e->getMessage());
            } catch (Exception $e) {
                alerta_error($e->getMessage());
            }
            ?>
        </div>
    </div>
    <?php
} catch (Exception $e) {
    alerta_error($e->getMessage(), "./index.php");
}


pies();