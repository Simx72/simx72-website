<?php
/*
 * Simx72 website
 * 
 * @author Simx72
 * @link mailto:angel2600@proton.me
 * @link http://sdesim.ca/
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 * -----
 * Copyright (C) 2024  Simx72
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 */
//

require_once __DIR__ . "/../include/template.php";
require_once __DIR__ . "/../include/db.php";
require_once __DIR__ . "/../include/auth.php";

template_config("Blog", __FILE__);


function mostrar_post(mixed $post, bool $editar = false)
{
	?>
	<div class="bloque mx-auto mb-3">
		<small>
			Fecha: &Tab;
			<?php echo $post["fecha_creado"]; ?> <br>
		</small>
		<div class="row">
			<div class="col-sm-9">
				<a class="sin-estilo" href="post.php?id=<?php echo dechex($post["id"]); ?>">
					<div class="titulito">
						<?php echo $post['titulo']; ?>
					</div>
					<div class="contenido-corto-pub">
						<?php
						try {
							$content = $post["content"];
							if (!$content == "") {
								// Create the lexer object with your given Quill JSON delta code (either PHP array or JSON string).
								$lexer = new \nadar\quill\Lexer($content);
								
								// Echo the HTML for the given JSON ops.
								$content = $lexer->render();

								$empty = trim(strip_tags($content)) == "";

								if ($empty) {
									$content = "[vacío]";
								} else {
									$firstLine = $lexer->getLine(0)->getLexer()->render();
									$content = $firstLine;
								}
							}
							echo $content;
						} catch (TypeError $e) {
							alerta_error("Ocurrió un error al mostrar esta publicación", "", "Al parecer la publicación esta defectuosa, inténtelo de nuevo mas tarde.<br>Error: ".$e->getMessage());
						} catch (Exception $e) {
							alerta_error($e->getMessage());
						}

						?>
					</div>
				</a>
			</div>
			<div class="col-sm-3">
				<div class="d-flex flex-sm-column align-items-end h-100 justify-content-end">
					<div>
						Por:
						<?php echo get_username_by_id($post["owner"]); ?>
						<div style="width: 2em; height: 2em" class="d-inline-block">
							<img src="../auth/imagen_perfil.php?id=<?php echo dechex($post["owner"]); ?>" alt=""
								class="img-de-perfil rounded-circle">
						</div>
					</div>
					<?php
					if ($editar) {
						?>
						<div>
							<a href="editar_post.php?id=<?php echo dechex($post["id"]); ?>"
								class="btn btn-info sin-estilo mt-2 ms-2">
								Editar
								<i class="bi bi-pen-fill"></i>
							</a>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
}

function get_my_hidden_posts()
{
	global $auth, $db;
	$owner = $auth->getUserId();
	$query_string = "SELECT * FROM `blog` WHERE `owner` = ? AND `hidden` = ?";
	$query = $db->prepare($query_string);
	$ok = $query->execute([$owner, true]);
	if ($ok) {
		$res = $query->fetchAll();
		return $res;
	} else {
		throw new Exception("Ocurrió un error con la consulta SQL: " . $query->errorInfo()[2]);
	}
}

$posts = blog_get_public_posts();


head();
?>
<div class="container-sm">
	<link rel="stylesheet" href="/build/css/blog.css">
	<?php
	if (auth_may_edit_blog()) {
		try {
			$mis_borradores = get_my_hidden_posts();
			?>
			<div class="titulo">Mis borradores</div>
			<a href="nuevo_post.php" class="btn btn-primary text-dark mb-2">
				Nuevo
				<i class="bi bi-file-earmark-plus-fill"></i>
			</a>
			<?php
			if (sizeof($mis_borradores) > 0) {
				foreach ($mis_borradores as $post) {
					mostrar_post($post, true);
				}
			} else {
				?>
				<div class="bloque mx-auto">
					No hay ningún borrador aún
				</div>
				<?php
			}
		} catch (Exception $e) {
			alerta_error($e->getMessage());
			error_log($e);
		}
		?>
		<div class="mb-5"></div>
		<?php
	}
	?>
	<div class="titulo">Publicaciones</div>
	<?php
	if (sizeof($posts) > 0) {
		foreach ($posts as $post) {
			$id = $auth->getUserId();
			mostrar_post($post, $id == $post["owner"]);
		}
	} else {
		?>
		<div class="bloque mx-auto my-1">
			<div class="row">
				<div class="col">
					No hay ninguna publicación aún
				</div>
				<?php
				if (auth_may_edit_blog()) {
					?>
					<div class="col justify-content-end d-flex">
						<a href="nuevo_post.php" class="btn btn-primary text-dark" style="text-align: right">
							Publicar algo
							<i class="bi bi-file-earmark-plus-fill"></i>
						</a>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<?php
	}
	?>
</div>
<?php

pies();